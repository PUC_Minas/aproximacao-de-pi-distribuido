package tp3;

import java.io.Serializable;

/**
 *
 * @author Paulo Leal
 */
public class Task implements Serializable{
    public int id;
    public double x;
    public double y;
    public boolean dentro;

    public Task(int id) {
        this.id = id;
    }
}
