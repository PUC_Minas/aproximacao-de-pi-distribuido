package tp3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paulo Leal
 */
public class Cliente {

    public static void main(String[] args) throws IOException {
        String serverAddr = args.length > 1 ? args[0] : "localhost";
        int serverPort = args.length > 2 ? Integer.parseInt(args[1]) : 8989;

        Socket server = new Socket(serverAddr, serverPort);
        ObjectOutputStream out = new ObjectOutputStream(server.getOutputStream());
        ObjectInputStream in = new ObjectInputStream(server.getInputStream());
        while (true) {
            try {                
                out.writeObject("solicitar");                
                System.out.println("Receber tarefa...");
                Task tarefa = (Task) in.readObject();
                if(tarefa.id == -1){
                    System.out.println("Não há tarefas disponíveis, aguardando 3s para nova requisição.");
					        try{
								System.out.println( "Pressione ENTER para encerrar..." );
								System.in.read( );
								System.exit( 0 );
							} catch ( IOException io ) {
								io.printStackTrace( );
								Thread.sleep(3000);
								continue;
							}
                }
                
                System.out.println("> Processando tarefa...");
                processarTarefa(tarefa);                        
                out.writeObject("responder"); 
				System.out.println("Calculo: sqrt(" + tarefa.x + "^" +2 + " + " + tarefa.y +"^" + 2 + ") = " + tarefa.dentro);
                System.out.println("> Enviar resposta...");
                out.writeObject(tarefa);

            } catch (ClassNotFoundException | InterruptedException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void processarTarefa(Task tarefa) {
        // Calcular distancia
        tarefa.x = getRandom();
        tarefa.y = getRandom();      
        
        tarefa.dentro = Math.sqrt( (tarefa.x * tarefa.x) + (tarefa.y * tarefa.y) ) <= 1;                
    }
    
    public static double getRandom(){
        double random = ( Math.random( ) * ( 2.0 - 0.0 ) ) + 0.0; //0-s0.99 -  1-2
        if( random < 1.0 ) {
            return ( random / 2.0 ) * -1.0; 
        } else return ( random / 2.0 );
    }

}
