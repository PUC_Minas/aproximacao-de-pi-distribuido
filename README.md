# Cálculo de $\pi$ utilizando abordagem distribuída

**Progresso:** Concluido<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2018<br />

### Objetivo
Implementar uma ferramenta para cálcular o valor de $\pi$ utilizando uma abordagem distribuída, para execução em diversas maquinas para obter o resultado.

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/)<br />
Linguagem: [JAVA 8](https://www.java.com/)<br />
Banco de dados: Não utiliza

### Execução

    $ Digite no script a quantidade de clientes
    $ .\start
    

### Contribuição

Esse projeto está finalizado e é livre para uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->