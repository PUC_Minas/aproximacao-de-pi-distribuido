ECHO TRABALHO DE COMPUTACAO DISTRIBUIDA
ECHO Implementacao do servidor de tarefas

SET CLIENTES=10
SET IP_SERVER="localhost"
SET PORTA_SERVIDOR=8989
SET QUANT_TAREFAS=1000000

ECHO Quantidade de clientes escolhida: %CLIENTES%
ECHO Quantidade de tarefas escolhida: %QUANT_TAREFAS%

ECHO Compilando cliente
CD Source
javac -encoding utf8 -d . *.java
jar cvfe ..\Cliente.jar tp3.Cliente tp3/*.class

ECHO Compilando servidor
jar cvfe ..\Servidor.jar tp3.Servidor tp3/*.class

ECHO Inicializando servidor
CD ..
START cmd.exe /k "TITLE SERVIDOR && java -jar Servidor.jar %PORTA_SERVIDOR% %QUANT_TAREFAS%"

ECHO Inicializando (%CLIENTES%) Clientes
SET /a "MAX_CLIENTES=%CLIENTES%-1"
FOR /L %%A IN (0,1,%MAX_CLIENTES%) DO (
  START cmd.exe /k "TITLE CLIENTE %%A && java -jar Cliente.jar %IP_SERVER% %PORTA_SERVIDOR%"
)